#!/usr/bin/env sh
#
# trashman handles junky software installation procedures
#
# Copyright (C) 2017 Silvio Rhatto - rhatto at riseup.net
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Parameters
NAME="trashman"
PROGRAM="$0"
BASENAME="`basename $0`"
ACTION="$1"
CWD="`pwd`"

# Set shared files location
if [ -e "`dirname $(readlink -f $0)`/share/$NAME" ]; then
  # Development or local installation layout
  APP_BASE="`dirname $(readlink -f $0)`"
  LIB="$APP_BASE/share/$NAME"
  SHARE="$APP_BASE/share/$BASENAME"
else
  # System installation layout
  APP_BASE="`dirname $(readlink -f $0)`"
  LIB="$APP_BASE/../share/$NAME"
  SHARE="$APP_BASE/../share/$BASENAME"
fi

# Include basic functions
. $LIB/trashman/functions || exit 1

# Display usage
trashman_usage() {
  echo "$BASENAME: package ports tree and heterodox configuration provisioner"
  echo ""
  echo "usage: $BASENAME <fetch|merge>"

  if [ "$BASENAME" = "trashman" ]; then
    echo "usage: $BASENAME <deploy> <hostname>"
  fi

  echo "usage: $BASENAME <action> [<package1> ... <packageN>] [<--param1=value1> ... <--paramM=valueM>]"
  echo ""
  echo "available packages:"
  echo ""

  for package in `trashman_implementations`; do
    if [ -e "$SHARE/$package/info" ]; then
      echo "\t $package: `cat $SHARE/$package/info` (`trashman_actions $package | sed -e 's/ /|/g'`)"
    fi
  done

  echo ""
  exit 1
}

# Dispatch
if [ -z "$ACTION" ]; then
  trashman_usage
elif [ "$ACTION" = "fetch" ]; then
  trashman_$ACTION
elif [ "$ACTION" = "merge" ]; then
  trashman_$ACTION
elif [ "$ACTION" = "deploy" ]; then
  trashman_$ACTION $*
elif [ "$ACTION" = "version" ]; then
  trashman_$ACTION
else
  # Get packages param
  shift
  PACKAGES="$*"

  # Setup packages
  if [ -z "$PACKAGES" ]; then
    trashman_usage
  else
    for package in $PACKAGES; do
      if [ ! -z "`trashman_actions $package`" ]; then
        folder="`trashman_actions_folder $package`"

        # Checking for privileges
        if [ "$BASENAME" = "trashman" ]; then
          trashman_check_root $ACTION $package
        else
          trashman_check_sudo
        fi

        if [ -x "$SHARE/$package/$folder/$ACTION" ]; then
          if [ "$ACTION" != "check" ] && [ "$ACTION" != "test" ]; then
            trashman_echo "Issuing action $ACTION for $package ($folder)..."
          fi

          # Run action for package
          $SHARE/$package/$folder/$ACTION $SHARE $LIB
          trashman_check_exit_status $ACTION $?
        else
          trashman_echo "No such action $ACTION for package $package, skipping"
        fi
      else
        trashman_echo "No such package $package, skipping"
      fi
    done

    # Exit with the latest return status
    exit $status
  fi
fi
