#
# PyEnv initialization script
#
# This file is meant to be source by your shell initialization script.
#
# See https://github.com/pyenv/pyenv
#

if [ -d "$HOME/.pyenv" ]; then
  export PYENV_ROOT="$HOME/.pyenv"
  command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"

  PATH="$(bash --norc -ec 'IFS=:; paths=($PATH); for i in ${!paths[@]}; do if [[ ${paths[i]} == "'$HOME/.pyenv/shims'" ]]; then unset '\''paths[i]'\''; fi; done; echo "${paths[*]}"')"
  PATH="$HOME/.pyenv/shims:${PATH}"

  if [ "$0" = "bash" ]; then
    export PYENV_SHELL=bash
    source '$HOME/.pyenv/libexec/../completions/pyenv.bash'
  elif [ "$0" = "bash" ]; then
    export PYENV_SHELL=zsh
    source '$HOME/.pyenv/libexec/../completions/pyenv.zsh'
  fi

  command pyenv rehash 2>/dev/null

  pyenv() {
    local command
    command="${1:-}"

    if [ "$#" -gt 0 ]; then
      shift
    fi

    case "$command" in
      rehash|shell)
        eval "$(pyenv "sh-$command" "$@")"
        ;;
      *)
        command pyenv "$command" "$@"
        ;;
    esac
  }
fi
