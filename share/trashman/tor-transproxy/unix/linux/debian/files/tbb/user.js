// Preferences for system-installed Tor Browser
// Needs either
//
//   * Setting TOR_CONTROL_PASSWORD at $TOR_BROWSER_PATH/start-tor-browser
//   * Passing TOR_CONTROL_PASSWORD to start-tor-browser via the command line
//
// See https://trac.torproject.org/projects/tor/wiki/TorBrowserBundleSAQ
//
user_pref("network.security.ports.banned",           "9050,9052");
user_pref("network.proxy.socks",                     "127.0.0.1");
user_pref("network.proxy.socks_port",                9050);
user_pref("extensions.torbutton.inserted_button",    true);
user_pref("extensions.torbutton.launch_warning",     false);
user_pref("extensions.torbutton.loglevel",           2);
user_pref("extensions.torbutton.logmethod",          0);
user_pref("extensions.torlauncher.control_port",     9052);
user_pref("extensions.torlauncher.loglevel",         2);
user_pref("extensions.torlauncher.logmethod",        0);
user_pref("extensions.torlauncher.prompt_at_startup",false);
user_pref("extensions.torlauncher.start_tor",        false);
