#!/bin/bash
#
# trashman basic functions.
#

# Make sure we're running git directly and not any existing wrapper
GIT="/usr/bin/git"
DIRNAME="$SHARE/../.."

# Message
trashman_echo() {
  #echo ""
  echo "-> $*"
}

# Message, no newline
trashman_echo_keepline() {
  #echo ""
  echo -n "-> $*"
}

# Our most common ancestor
trashman_ancestor() {
  echo "unix"
}

# Determine OS family
trashman_family() {
  uname | tr '[:upper:]' '[:lower:]'
}

# Determine distro name
trashman_distro() {
  local uname="`trashman_family`"

  if [ -e "/etc/os-release" ]; then
    grep "^ID=" /etc/os-release | cut -d '=' -f 2
  elif [ "$uname" = "linux" ]; then
    if [ -e "/etc/debian_version" ]; then
      echo "debian"
    else
      echo "linux"
    fi
  fi
}

# Distro release version
trashman_distro_release() {
  if [ -e "/etc/os-release" ]; then
    #grep "^VERSION=" /etc/os-release | cut -d '(' -f 2 | cut -d ')' -f 1
    grep "^VERSION_CODENAME=" /etc/os-release | cut -d '=' -f 2
  fi
}

# Machine architecture
trashman_arch() {
  uname -m
}

# Require a program available on $PATH
trashman_require() {
  if [ -z "$1" ]; then
    return
  fi

  for program in $*; do
    if ! which $program > /dev/null 2>&1; then
      trashman_echo "Requirement absent: $program"
      exit 1
    fi
  done
}

# Return the folder where actions are available for a package
trashman_actions_folder() {
  local package="$1"
  local actions=""
  local ancestor="`trashman_ancestor`"
  local family="`trashman_family`"
  local distro="`trashman_distro`"
  local findopts="-maxdepth 1 -type f -executable -follow -exec basename {}"

  if [ -z "$package" ]; then
    return
  fi

  if [ ! -d "$SHARE/$package/$ancestor" ]; then
    return
  fi

  if [ -e "$SHARE/$package/$ancestor/$family/$distro" ]; then
    actions="`find $SHARE/$package/$ancestor/$family/$distro $findopts \; 2> /dev/null | xargs`"
  else
    actions=""
  fi

  if [ -z "$actions" ]; then
    if [ -e "$SHARE/$package/$ancestor/$family" ]; then
      actions="`find $SHARE/$package/$ancestor/$family $findopts \; 2> /dev/null | xargs`"
    else
      actions=""
    fi

    if [ -z "$actions" ]; then
      actions="`find $SHARE/$package/$ancestor $findopts \; 2> /dev/null | xargs`"

      if [ ! -z "$actions" ]; then
        echo $ancestor
      fi
    else
      echo $ancestor/$family
    fi
  else
    echo $ancestor/$family/$distro
  fi
}

# Return the list of available actions from a package
trashman_actions() {
  local package="$1"
  local findopts="-maxdepth 1 -type f -executable -follow -exec basename {}"

  if [ -z "$package" ]; then
    return
  fi

  local folder="`trashman_actions_folder $package`"

  if [ ! -z "$SHARE/$package/$folder" ]; then
    find $SHARE/$package/$folder $findopts \; 2> /dev/null | xargs
  fi
}

# Return list of implementations
trashman_implementations() {
  local actions=""
  local implementations=""

  for package in `ls $SHARE`; do
    actions="`trashman_actions $package`"

    if [ ! -z "$actions" ]; then
      implementations="$implementations $package"
    fi
  done

  echo $implementations
}

# Version information
trashman_version() {
  echo "master branch:"
  echo "=============="
  echo ""
  ( cd $DIRNAME && $GIT log --show-signature -n 1 )

  echo ""
  echo "origin/master branch:"
  echo "====================="
  echo ""
  ( cd $DIRNAME && $GIT log --show-signature -n 1 --remotes --branches=origin/master )
}

# Fetch
trashman_fetch() {
  ( cd $DIRNAME && $GIT fetch --all && $GIT log --show-signature -n 1 --remotes --branches=origin/master )
}

# Merge
trashman_merge() {
  (
  cd $DIRNAME && $GIT merge origin/master                 && \
                 $GIT submodule sync          --recursive && \
                 $GIT submodule update --init --recursive
  )
}


trashman_check_exit_status() {
  local action="$1"
  local status="$2"

  if [ -z "$status" ]; then
    return
  fi

  if [ "$action" = "check" ]; then
    if [ "$status" = "0" ]; then
      trashman_echo "Package $package is installed system-wide"
    elif [ "$status" = "1" ]; then
      trashman_echo "Package $package is not installed system-wide"
    elif [ "$status" = "2" ]; then
      trashman_echo "Package $package is partially installed system-wide"
    fi
  fi

  if [ "$action" = "test" ]; then
    if [ "$status" = "0" ]; then
      trashman_echo "Package $package is running as expected"
    elif [ "$status" = "1" ]; then
      trashman_echo "Package $package is not running as expected"
    fi
  fi
}

# Check for superuser privileges
trashman_check_root() {
  local action="$1"
  local package="$2"

  if [ -z "$action" ]; then
    return
  fi

  if [ "$action" != "install" ] && [ "$action" != "remove" ] && [ "$action" != "upgrade" ]; then
    return
  fi

  if [ "`whoami`" != "root" ]; then
    if which sudo > /dev/null 2>&1; then
      # Try to invoke it with sudo
      trashman_echo "You're running as a regular user, trying to re-invoke the command using sudo..."
      sudo $PROGRAM $action $package
      exit $?
    else
      trashman_echo "Action $action requires root privileges"
      exit 1
    fi
  fi
}

# Check for sudo
trashman_check_sudo () {
  if [ "`whoami`" != "root" ]; then
    export SUDO="sudo"
  fi
}

# Deploy to a remote server
trashman_deploy () {
  SERVER="$2"
  RSYNC_PATH="sudo rsync"
  DEPLOY_RSYNC="rsync -rltDv --no-perms --no-owner --delete --rsync-path"
  SSH="ssh -T -q -o ConnectTimeout=15"

  trashman_echo "Deploying to $SERVER..."

  # Check remote environment
  $SSH $SERVER <<EOF
  ##### BEGIN REMOTE SCRIPT #####
  # Check for passwordless sudo
  # See references like https://raymii.org/s/articles/Check_if_passwordless_sudo_can_be_used_in_a_bash_script_or_nagios_check.html
  if ! sudo -n true; then
    echo "Please set passwordless sudo on $SERVER."
    echo "You can do that by adding the following line on /etc/sudoers.d/local:"
    echo ""
    echo "%sudo ALL=NOPASSWD: ALL"
    echo ""
    echo "And make sure your user is included in the 'sudo' group."
    exit 1
  fi
  ##### END REMOTE SCRIPT #######
EOF

  # Check connection
  if [ "$?" != "0" ]; then
    trashman_echo "Error connecting or setting up $FQDN."
    exit 1
  fi

  $DEPLOY_RSYNC "$RSYNC_PATH" $APP_BASE/ $SERVER:/usr/local/share/trashman/
  $SSH $SERVER sudo /usr/local/share/trashman/trashman install trashman
}

# Inform where a package was installed
trashman_installed_at () {
  echo "Package installed at $*"
}

# Inform how to run the package main command
trashman_run_with () {
  echo "Run with $*"
}
