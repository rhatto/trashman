# Ideas

* Vendorization support.
* Support for installing from remote repositories (custom channels).
* Support more OSes at `__trashman_distro()`.
* Argument passing to distro/action scripts as `--param=value`.
* Do a `check` action (if available) before testing/installing/removing.
* Add a `run` action as a way to run an installed package executable, like
  `hoarder run tor-browser-alpha`.
* Multi-licensing and note on the lack of licensing for some scripts.
* Import action to create new packages from a source URI.
* Install from git remote URL (try to guess how to install the software).
* Makefile with DESTDIR (install, uninstall).
* Remote deployment and execution of trashman.
* Dependency management:
  * File `dependency` for each package, one package declaration per line.
  * Package declaration in the form of `<trashman|hoarder> name`.
  * Example: `hoarder/irpf2018` depends on `trashman/oracle-java8`.
* New packages:
  * hoarder:
    * rtorrent-ps
    * trashman (to install it locally)
    * Tor Browser
    * Tor Browser Alpha
    * Mullvad Browser
    * Brave Browser
  * trashman:
    * pkgtool
    * simplepkg etc
    * [pkgutils](https://github.com/nipuL/pkgutils)
    * caddy http server
    * kvmx (using it's Makefile?)
