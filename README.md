Trashman: the dystopic package and config manager
=================================================

    Poorman's Rule:
            When you pull a plastic garbage bag from its handy dispenser
            package, you always get hold of the closed end and try to
            pull it open.

Hello dumpster divers! Wanna know about the new tool in the "ecosystem"?

It pollutes your environment with the modern depressing stack like nodejs and
docker.

Some stuff are hard to package or upstream makes the effort to keep it harder
and harder to package. Distros are left behind the planned obsolescence.

This humble tool alleviates the pain like if you find yourself in the urge to
install that hyped application but have no choice other than run magic commands
and install untrusted code.

It replaces curl-pipe-bash statements like

    curl $backdoor_url | bash

by this one:

    trashman install <package>

Instead of being just a shorthand, `trashman` packages can try as best as possible
not to fetch keys and scripts from remote locations without some basic checks. So if
you have to add files into `/etc/apt/trusted.gpg.d`, `trashman` will provide those
keys or fingerprints instead of downloading it from somewhere, in which case it could
be easily tampered.

## Hoarder

Hoarder is trashman's companion, an equivalent script intended to install packages
locally, at the current user's folder:

    hoarder install <package>

## Features

* Multi-arch, multi-OS: you might code packages for any distro,
  be it a GNU/Linux flavour, BSD, Minix or whatever UNIX implementation.
  Its written in bourne shell language, which guarantees it may run in most
  systems.

* Multi-language: code your package in the language you want, the way you want.
  This tool is mainly a script dispatcher. Packages have only to respect some
  basic conventions like exit statuses.

* Stateless: it doesnt save package state. Instead, packages should provide their
  own built-in code to detected whether they are installed, half-installed or not.

* Installess: `trashman` itself does not, but can be, installed system-wide.
  It has no other dependencies other than basic tools like those available
  in coreutils packages and nowadays common applications like `git`.

* Provides basic automation: instead of always running command by hand, recipes
  can be included in your deployment scripts.

* The one to rule then all; a meta package manager suportting any other
  package manager, like apt/dpkg, stowpkg, pkg_src, etc.

## Limitations

* It is not a full-feature package manager. Use the one provided by your distro
  instead. We're not advocating you to stop using your distro way to manage things.
  We're just providing an alternative way when your distro does not provide easier
  ways to install some software.

* Do not use your main system for installing `trashman` packages. Use a Virtual Machine
  instead: this is more manageable and keep the litter isolated from the other parts
  of your infrastructure.

* Not everything can be checked in advance by `trashman` packages. Each package can do
  it's best to check sources and avoiding running unsigned/untrusted code from remote
  locations.

* This software is a poor mitigation and a way to save yourself some time in a growing
  trend of open source software lifecycles dominated by conglomerates running their
  "cloud" providing you with "open core" applications, taking over your computing stack.

## Installation

Simply clone it and add to your `$PATH`:

    git clone https://git.fluxo.info/trashman

You can also verify the latest commit's OpenPGP signature:

    /usr/bin/git -C templater verify-commit HEAD

Note that `/usr/bin/git` is called to avoid any other `git` wrappers or aliases
you might have available on your shell.

You might also proceed with a system-wide install using `trashman` do install
itself in your system:

     ./trashman install trashman

## Syncing the codebase

Syncing the codebase is a two-step procedure. First grab upstream changes:

    trashman fetch

This will output current OpenPGP signature's from the last commit. You might
check that and also check for repository changes. Once you're fine with those,
do the actual merge:

    trashman merge

## How it works

Basically `trashman` is just a dispatcher/router for scripts available under
it's `share/` directory. It scans every available package for the available
implementation that best fits your current distro.

Say that you're running Debian GNU/Linux. First trashman tries to find scripts
that matches `debian` distro, trying `linux` target next and finally `unix`.

This ways, it's possible to implement generic package management procedures
as longs as specific to each distro.

## Writing a package

You might use [templater](https://templater.fluxo.info):

    cd path/to/trashman/repo && templater share/package/<package_name> trashman

packages are simply as having the following files:

* `share/trashman/<package>/info`: describe package purpose.
* `share/trashman/<package>/<ancestor>/<family>/<distro>/<action>`: script that runs on a given action.
* `share/trashman/<package>/<ancestor>/<family>/<action>`: script that runs on a given action, fallback.
* `share/trashman/<package>/<ancestor>/<action>`: script that runs on a given action, fallback.

Where actions can be like `install`, `check`, `remove` or `upgrade`. You don't have
to implement all actions. Actually, no action is required to create a package, but
having no action makes it useless.

### Check action

The `check` action may return the following exit codes:

* 0: the package is installed system-wide.
* 1: the package is not installed system-wide.
* 2: the package is partially installed system-wide.

Yes. This is far from deterministic builds. We're talking about scripts that might fail
anytime, due to network errors or inconsistencies.

### Idempotency

Shit. I think I can't even spell this word correctly. Idempowhat? So you're saying
you want to make your program keep your system in the same state no matter how many
times you run it?

You don't need a DSL to do that. It's as simply as

    if ! installed $program; then
      install_program $program
    fi

It's recommended that you try to make your packaging script to be idempotent.

This way,`trashman` turns into a configuration manager, going beyond the basic
package management.

## Naming candidates

While designing this tools, the following names were considered:

* trashman
* garbageman
* dumpster
* dumpsterize
* gari
* lixomanina
* trashops

This software is dedicated to bands like Trashmen, Trashwomen and Lixomania.
